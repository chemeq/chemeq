package net.oelen.chemeq.math;

import java.math.BigInteger;


// This is a class, which can be used to recombine linearly independent
// vectors, such that all vectors only have non-negative elements.
// This is useful for building chemical equations. We want coefficients
// to be all positive. For multi-dimensional solution spaces, the base
// vectors can have negative elements. Any linear combination of the
// independent base vectors still span the same solution space and this
// is used to try to find suitable vectors. An example is given below.
//
// We have solution space, specified by the following two columns
//     1   3
//     2  -3
//     1   1
//     2   1
//
// This solution has a negative element in the second column. We can
// add 2 times the first column to the second one. This does not
// change the solution space. So, we recombine the columns and get
//     1   5
//     2   1
//     1   3
//     2   5
//
// Now we have all positive coefficients. The reaction equations
// now only have positive coefficients and that is what the end
// user wants.
public class Recombine {

    static final int STRICTLY_NEGATIVE = -2;
    static final int NEGATIVE = -1;
    static final int MIXED = 0;
    static final int POSITIVE = 1;
    static final int STRICTLY_POSITIVE = 2;

    
    
    
    // Given a matrix M with multiple columns, a new matrix M
    // with the same number of columns is computed, such that
    // it only has non-negative elements, by recombining
    // columns, as described in the comment above.
    public static Matrix recombine(Matrix M) {

        // Search depths (sum of absolute value of all multiples in
        // the linear combination) for different number of columns.
        // We only do this search for situations where the number
        // of columns is not too large.
        int searchDepth[] = new int[]{0, 0, 0, 50, 30, 20, 15};

        if (M.getNCols() == 1 || M.getNCols() >= searchDepth.length) {
            return M;
        }

        // If we only have two columns, then we can use a specialized
        // more efficient and more certain recombining method.
        if (M.getNCols() == 2) {
            return recombine2(M);
        }

        int nColumnsOK = 0;
        int strictColIndex = -1;
        int[] columnSpec = new int[M.getNCols()];
        for (int col = 0; col < M.getNCols(); col++) {
            columnSpec[col] = MIXED;
            int colType = columnType(M, col);
            if (colType != MIXED) {
                nColumnsOK++;
                if (colType < 0) {
                    columnNegate(M, col);
                }
                columnSpec[col] = POSITIVE;
                if (colType == STRICTLY_POSITIVE || colType == STRICTLY_NEGATIVE) {
                    strictColIndex = col;
                }
            }
        }

        if (nColumnsOK == M.getNCols()) {
            return M;
        }

        if (strictColIndex >= 0) {
            // We can make all columns suitable simply by adding a sufficiently
            // high multiple of the strictly positive column.
            // TODO:
        }

        nColumnsOK = 0;
        Matrix R = new Matrix(M.getNRows(), M.getNCols());
        for (int col = 0; col < M.getNCols(); col++) {
            if (columnSpec[col] == POSITIVE) {
                for (int row = 0; row < M.getNRows(); row++) {
                    R.setElement(row, nColumnsOK, M.getElement(row, col));
                }
                nColumnsOK++;
            }
        }

        // We use a fairly dumb method of recombining. We systematically try
        // all kinds of combinations of recombining them, the only intelligence
        // being used is that recombinations, which are a multiple of previously
        // tried recombinations are not checked anymore. These are not linearly
        // independent from what already is tried before and add no new info.
        Sequencer sequencer = new Sequencer(columnSpec, searchDepth[M.getNCols()]);
        int[] linearCombination;
        while ((nColumnsOK < M.getNCols()) && (linearCombination = sequencer.next()) != null) {
            buildColumn(nColumnsOK, R, M, linearCombination);
            int colType = columnType(R, nColumnsOK);
            if (colType != MIXED) {
                if (colType < 0) {
                    columnNegate(R, nColumnsOK);
                    colType = -colType;
                }
                sequencer.registerAsUsed(linearCombination);
                columnNormalize(R, nColumnsOK);
                nColumnsOK++;
            }
        }

        return (nColumnsOK == M.getNCols()) ? R : M;
    }

    
    
    
    static private int columnType(Matrix M, int col) {
        int pos = 0;
        int neg = 0;
        for (int row = 0; row < M.getNRows(); row++) {
            if (M.getElement(row, col).sign() > 0) {
                pos++;
            }
            if (M.getElement(row, col).sign() < 0) {
                neg++;
            }
        }

        if (pos > 0 && neg == 0) {
            return (pos == M.getNRows()) ? STRICTLY_POSITIVE : POSITIVE;
        }
        if (neg > 0 && pos == 0) {
            return (neg == M.getNRows()) ? STRICTLY_NEGATIVE : NEGATIVE;
        }
        return MIXED;
    }

    
    
    
    private static void columnNegate(Matrix M, int col) {
        for (int row = 0; row < M.getNRows(); row++) {
            M.setElement(row, col, M.getElement(row, col).minus());
        }
    }

    
    
    
    private static void columnNormalize(Matrix M, int col) {
        BigInteger gcd = BigInteger.ZERO;
        for (int row = 0; row < M.getNRows(); row++) {
            gcd = gcd.gcd(M.getElement(row, col).getNum());
        }
        for (int row = 0; row < M.getNRows(); row++) {
            M.setElement(row, col, M.getElement(row, col).getNum().divide(gcd));
        }
    }

    
    
    
    
    
    
    private static void buildColumn(int col, Matrix R, Matrix M, int[] linearCombination) {
        for (int row = 0; row < R.getNRows(); row++) {
            R.setElement(row, col, Rational.ZERO);
            for (int i = 0; i < linearCombination.length; i++) {
                if (linearCombination[i] == 0) {
                    continue;
                }
                BigInteger term = M.getElement(row, i).getNum();
                if (linearCombination[i] == 1) {
                    R.setElement(row, col, new Rational(R.getElement(row, col).getNum().add(term)));
                } 
                else if (linearCombination[i] == -1) {
                    R.setElement(row, col, new Rational(R.getElement(row, col).getNum().subtract(term)));
                } 
                else {
                    term = term.multiply(BigInteger.valueOf(linearCombination[i]));
                    R.setElement(row, col, new Rational(R.getElement(row, col).getNum().add(term)));
                }
            }
        }
    }

    
    
    // Checks whether two vectors (two columns in a N-row, 2-column
    // matrix) are linearly independent.
    private static boolean indep_2vectors(Matrix M) {
        int r;
        for (r = 0; r < M.getNRows(); r++) {
            if (M.getElement(r, 0).sign() == 0) {
                if (M.getElement(r, 1).sign() == 0) {
                    // Both elements are zero. Such an element cannot be
                    // used for testing and we advance to the next one.
                    continue;
                }

                return true;
                // They are independent.
            } 
            else if (M.getElement(r, 1).sign() == 0) {
                return true;
                // They are independent.
            } 
            else {
                BigInteger gcd, fac0, fac1;
                gcd = M.getElement(r, 0).getNum().gcd(M.getElement(r, 1).getNum());
                fac0 = M.getElement(r, 1).getNum().divide(gcd);
                fac1 = M.getElement(r, 0).getNum().divide(gcd);

                for (int rr = 0; rr < M.getNRows(); rr++) {
                    if (M.getElement(rr, 0).getNum().multiply(fac0).subtract(M.getElement(rr, 1).getNum().multiply(fac1)).signum() != 0) {
                        return true;
                    }
                }
                return false;
            }
        }

        return false;
    }
    
    
    
    

    // Recombine a matrix, consisting of 2 integer vectors, such that a
    // basis is obtained with positive elements only. Such a basis only
    // is determined if it is possible.
    // It is assumed that the matrix has 2 columns and that all elements
    // are integers (i.e. the denominator equals 1 for all elements).
    private static Matrix recombine2(Matrix M) {
        Matrix MM = new Matrix(M.getNRows(), 2);

        int c = 0;
        for (int r = 0; r < M.getNRows() && c < 2; r++) {
            int rr;

            // Determine a linear combination of the vector N(*:0) and N(*:1)
            // which has a zero element at position r. We do this by computing
            // the LCM of the elements of both vectors at position r.
            if (M.getElement(r, 0).sign() == 0) {
                if (M.getElement(r, 1).sign() == 0) {
                    // Both elements are zero. Such an element cannot
                    // be accepted and we advance to the next one.
                    continue;
                }

                // We simply copy the current vector at column 0.
                for (rr = 0; rr < M.getNRows(); rr++) {
                    MM.setElement(rr, c, M.getElement(rr, 0));
                }
            } 
            else if (M.getElement(r, 1).sign() == 0) {
                // We simply copy the current vector at column 1.
                for (rr = 0; rr < M.getNRows(); rr++) {
                    MM.setElement(rr, c, M.getElement(rr, 1));
                }
            } 
            else {
                BigInteger gcd, fac0, fac1;
                BigInteger mr0 = M.getElement(r, 0).getNum();
                BigInteger mr1 = M.getElement(r, 1).getNum();
                gcd = mr0.gcd(mr1);
                fac0 = mr1.divide(gcd);
                fac1 = mr0.divide(gcd);

                for (rr = 0; rr < M.getNRows(); rr++) {
                    MM.setElement(rr, c, new Rational(M.getElement(rr, 0).getNum().multiply(fac0).subtract(M.getElement(rr, 1).getNum().multiply(fac1))));
                }
            }

            // Determine whether all elements of the linear combination
            // have the same sign or a zero sign. A vector containing
            // both positive and negative elements is not accepted.
            // A vector containing zeros only also is not accepted.
            int columnType = columnType(MM, c);

            if (columnType != MIXED) {
                // If the sign is negative then we make all elements positive.
                if (columnType < 0) {
                    columnNegate(MM, c);
                }

                // We have found a vector, with a zero at the
                // dependent compound position and all other
                // elements of the vector of the same sign.
                // If c == 0, then we simply accept this
                // vector. If c == 1, then we only accept it,
                // if it is not linearly dependent on the
                // vector, we found earlier for c == 0.
                if (c == 0) {
                    // Accept the vector by incrementing the
                    // number of suitable vectors found.
                    columnNormalize(MM, c);
                    c++;
                } 
                else {
                    if (indep_2vectors(MM)) {
                        // The new vector is independent of the other one,
                        // we accept it by incrementing the number c.
                        columnNormalize(MM, c);
                        c++;
                    }
                }
            }
        }

        return (c == 2) ? MM : M;
    }
}

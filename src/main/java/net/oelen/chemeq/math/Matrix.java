package net.oelen.chemeq.math;

import java.math.BigInteger;

public class Matrix {

    private int nRows, nCols;
    final private Rational m[][];

    
    
    private Matrix(Matrix mat) {
        nRows = mat.nRows;
        nCols = mat.nCols;
        m = new Rational[nRows][nCols];
        for (int i = 0; i < nRows; i++) {
            for (int j = 0; j < nCols; j++) {
                m[i][j] = mat.m[i][j];
            }
        }
    }

    
    
    
    Matrix copy() {
        Matrix newM = new Matrix(this);
        return newM;
    }

    
    
    
    
    public Matrix(int r, int c) {
        nRows = r;
        nCols = c;
        m = new Rational[r][c];
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                m[i][j] = Rational.ZERO;
            }
        }
    }

    
    
    
    
    public void setRow(int r, int[] row) {
        for (int c = 0; c < row.length && c < nCols; c++) {
            m[r][c] = new Rational(row[c]);
        }
    }

    
    
    
    
    public void setElement(int r, int c, long val) {
        m[r][c] = new Rational(val);
    }

    
    
    
    
    public void setElement(int r, int c, BigInteger val) {
        m[r][c] = new Rational(val);
    }

    
    
    
    public void setElement(int r, int c, Rational val) {
        m[r][c] = val;
    }

    
    
    
    
    public Rational getElement(int r, int c) {
        return m[r][c];
    }

    
    
    
    public int getNRows() {
        return nRows;
    }

    
    
    
    public int getNCols() {
        return nCols;
    }

    
    
    
    /*
     * here 0 <= i <= j < nRows.
     * returns the row index k, with least k (i <= k < nRows), such that 
     * m[k][j] is non-zero; returns nRows otherwise.
     */
    private int pivot(int i, int j) {
        int k = i;
        while (k < nRows && m[k][j].sign() == 0) {
            k++;
        }
        return k;
    }

    
    
    
    
    /*
     * 0 <= p < q < nRows.
     * exchanges the p-th and q-th rows, but
     * only do so for the part of the columns
     * starting at column p. The other elements
     * are assumed to be 0.
     */
    private void trans(int p, int q) {
        for (int j = p; j < nCols; j++) {
            Rational tmp = m[p][j];
            m[p][j] = m[q][j];
            m[q][j] = tmp;
        }
    }

    
    
    
    /*
     * divides the p-th row by m[p][j].
     * 0 <= p < nRows.
     * 0 <= j < nRows.
     * Assumption: m[p][k] = 0 if k < j.
     */
    private void normalize_leading_coefficient(int p, int j) {
        Rational x = m[p][j];

        if (!x.isOne()) {
            m[p][j] = Rational.ONE;
            for (int k = j + 1; k < nCols; k++) {
                m[p][k] = m[p][k].div(x);
            }
        }
    }

    
    
    
    
    /*
     * subtracts a times the p-th row from the q-th row,
     * for columns nCols > k > j.
     * 0 <= p, q < nRows.
     * Assumption: m[p][k] = 0 if k < j, m[p][j] = 1.
     */
    private void subtract_pivot_row(int pivotRow, int row, int pivotCol) {
        Rational factor = m[row][pivotCol];
        if (factor.sign() == 0) {
            // Nothing needs to be done, we have to subtract 0 times
            // the pivot row from the other row.
            return;
        }

        // We subtract the pivot row, multiplied by m[row][pivotCol]
        // from the other row. As the pivot leading coefficient equals
        // 1, this means that m[row][pivotCol] becomes 0.
        m[row][pivotCol] = Rational.ZERO;
        for (int col = pivotCol + 1; col < nCols; col++) {
            Rational term = m[pivotRow][col].mul(factor);
            m[row][col] = m[row][col].sub(term);
        }
    }

    
    
    
    
    /*
     * Determines the reduced row-echelon form of the current Matrix 'this'.
     * The current matrix is overwritten with the reduced echelon form.
     * The reduced echelon form has the same null-space as the original
     * matrix.
     * In linear algebra a matrix is in row echelon form if
     *      1) All nonzero rows are above any rows of all zeroes, and
     *      2) The leading coefficient of a row is always strictly to
     *         the right of the leading coefficient of the row above it.
     *      3) The leading coefficient of each nonzero row is equal to 1.
     *
     * A matrix is in _reduced_ row echelon form (also called row canonical form)
     * if it satisfies the above three conditions, and if, in addition
     *      4) Every leading coefficient is the only nonzero entry in its column.
     *         The first non-zero entry in each row is called a pivot.
     *         
     * Besides determining the reduced row-echelon form of the matrix,
     * this method returns the rank of the current Matrix, abd 
     * C[0],...,C[rank-1] are filled with the index of the columns in 
     * which the leading entries 1 occur.
     */
    private int to_reduced_row_echelon_form(int[] C) {
        int A[] = null;

        if (C != null) {
            A = new int[nCols];
            for (int i = 0; i < nCols; i++) {
                A[i] = 1;
            }
        }

        int row = 0;
        int col = 0;
        while (true) {
            while ((col < nCols) && (pivot(row, col) == nRows)) {
                col++;
            }

            if (col == nCols) {
                break;
            }

            // Assure that leading coefficients are never below
            // the diagonal of the matrix. They are either above
            // the diagonal or on the diagonal. This is assured
            // by exchanging rows (this is an elementary matrix
            // transformation, which does not affect the null space).
            int p = pivot(row, col);
            if (p > row) {
                // Echange the two rows.
                trans(row, p);
            }

            // Assure that the leading coefficient of the row equals 1.
            normalize_leading_coefficient(row, col);

            // For the current pivot row, record the column index
            // of the leading coefficient. Each column index, for
            // which there is a pivot row is also recorded, by making
            // array A at that column index equal to 0.
            if (C != null) {
                C[row] = col;
                A[col] = 0;
            }

            // For all rows not equal to the the pivot row, subtract 
            // the pivot row such that the elements in all other rows,
            // in the same column as the pivot, equals 0. Subtracting
            // a certain row from other rows does not affect the null
            // space, this also is an elementary matrix transformation.
            for (int r = 0; r < nRows; r++) {
                if (r != row) {
                    subtract_pivot_row(row, r, col);
                }
            }

            row++;
            col++;

            // If we are either beyond the last row, or beyond
            // the last column, then bail out of the loop.
            if (row == nRows || col == nCols) {
                break;
            }
        }

        // The rank of the matrix is equal to the row-number.
        int rank = row;

        // All column indexes for which there is a row with the
        // column index as pivot index are recorded in the array
        // C by now. The other coluns, for which no pivot is 
        // recorded, are now put in the remaining elements of
        // C, starting at C[rank] up to C[nCols-1].
        if (C != null) {
            for (int j = rank; j < nCols; j++) {
                for (int i = 0; i < nCols; i++) {
                    if (A[i] != 0) {
                        C[j] = i;
                        A[i] = 0;
                        break;
                    }
                }
            }
        }

        return rank;
    }

    
    
    
    
    public int rank() {
        Matrix N = this.copy();
        return N.to_reduced_row_echelon_form(null);
    }

    
    
    
    
    /*
     * returns a matrix whose columns form a basis for 
     * the null-space of 'this'. If the dimension of
     * the null-space equals 0, then returns NULL.
     */
    public Matrix getKernel() {
        int i, j, r;

        int[] C = new int[nCols];
        Matrix N = this.copy();
        r = N.to_reduced_row_echelon_form(C);
        if (r == nCols) {
            return null;
        }

        Matrix L = new Matrix(nCols, nCols - r);
        for (i = 0; i < r; i++) {
            for (j = 0; j < nCols - r; j++) {
                L.m[C[i]][j] = N.m[i][C[r + j]].minus();
            }
        }

        for (i = r; i < nCols; i++) {
            for (j = 0; j < nCols - r; j++) {
                L.m[C[i]][j] = (j == i - r) ? Rational.ONE : Rational.ZERO;
            }
        }

        // Make integers from all columns of L by scaling the 
        // columns with the LCM of all denominators in the row.
        // The first non-zero element of a base vector is made positive.
        for (i = 0; i < L.nCols; i++) {
            BigInteger lcm = BigInteger.ONE;
            int sign = 0;
            for (j = 0; j < L.nRows; j++) {
                lcm = L.m[j][i].denLcm(lcm);
                if (sign == 0) {
                    sign = L.m[j][i].sign();
                }
            }

            Rational totalLcm = new Rational((sign == -1) ? lcm.negate() : lcm);
            for (j = 0; j < L.nRows; j++) {
                L.m[j][i] = L.m[j][i].mul(totalLcm);
            }
        }

        return L;
    }

    /* Uncomment for testing purposes.
    
    public Matrix multiply(Matrix N) {
        Matrix M = null;
        if (nCols != N.nRows) {
            return M;
        }
        M = new Matrix(nRows, N.nCols);
        for (int r = 0; r < nRows; r++) {
            for (int c = 0; c < N.nCols; c++) {
                Rational elem = Rational.ZERO;
                for (int k = 0; k < nCols; k++) {
                    elem = elem.add(m[r][k].mul(N.m[k][c]));
                }
                M.m[r][c] = elem;
            }
        }

        return M;
    }
    
    
    
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Supply the input test file as first arguemnt");
            return;
        }

        try (BufferedReader in = new BufferedReader(new FileReader(args[0]))) {
            int nRows = 0, nCols = 0;
            Matrix mat = null;

            String str;
            while (mat == null && (str = in.readLine()) != null) {
                str = str.trim();
                if (!str.equals("")) {
                    if (nRows == 0) {
                        nRows = Integer.parseInt(str);
                        continue;
                    }
                    if (nCols == 0) {
                        nCols = Integer.parseInt(str);
                    }
                    if (nRows > 0 && nCols > 0) {
                        mat = new Matrix(nRows, nCols);
                    }
                }
            }

            if (mat != null) {
                int idx = 0;

                while (idx < nRows * nCols && (str = in.readLine()) != null) {
                    str = str.trim();
                    if (!str.equals("")) {
                        int val = Integer.parseInt(str);
                        mat.setElement(idx / nCols, idx % nCols, val);
                        idx++;
                    }
                }
                Matrix N = mat.getKernel();
                int rank = mat.rank();
                Matrix R = Recombine.recombine(N);

                System.out.println("Matrix MAT: " + mat.getNRows() + "*" + mat.getNCols() + ", rank equals " + rank);
                for (int i = 0; i < mat.nRows; i++) {
                    for (int j = 0; j < mat.nCols; j++) {
                        Rational val = mat.getElement(i, j);
                        System.out.print(" " + val.getNum().longValue());
                    }
                    System.out.println("");
                }
                System.out.println("");

                if (N != null) {
                    System.out.println("Matrix N: " + N.getNRows() + "*" + N.getNCols());
                    for (int i = 0; i < N.nRows; i++) {
                        for (int j = 0; j < N.nCols; j++) {
                            Rational val = N.getElement(i, j);
                            System.out.print(" " + val.getNum().longValue());
                        }
                        System.out.println("");
                    }
                    System.out.println("");

                    System.out.println("Matrix R: " + R.getNRows() + "*" + R.getNCols());
                    for (int i = 0; i < R.nRows; i++) {
                        for (int j = 0; j < R.nCols; j++) {
                            Rational val = R.getElement(i, j);
                            System.out.print(" " + val.getNum().longValue());
                        }
                        System.out.println("");
                    }

                } 
                else {
                    System.out.println("Null space has dimension 0.");
                }
            }
        } 
        catch (IOException e) {
            System.out.println("Error while reading input file.");
        }
    }
    
    */
}

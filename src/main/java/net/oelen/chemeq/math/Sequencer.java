package net.oelen.chemeq.math;

class SequencerForOneN {

    final private int[] multiples;

    private int signMask;
    private int nColumnsWithFreeSign;
    final private int[] indexOfColumnsWithFreeSign;
    final private int[] multiplesSigned;

    private int nAlreadyUsed;
    final private int[][] alreadyUsedLinearCombination;

    // This holds an array of integers, one integer for each column of
    // a matrix. If a column only has non-negative elements, then
    // a value POSITIVE is put in the columnSpec at the index of the column,
    // otherwise a value MIXED is put in the columnSpec.
    // This is used to determine whether a combination of vectors
    // is useful or not. A linear combination of only nonnegative
    // vectors does not add anything interesting. Only linear
    // combinations of vectors with negative elements and other
    // vectors can add a basis vector, which has non-negative
    // elements only.
    final private int[] columnSpec;

    
    
    
    public SequencerForOneN(int[] columnSpec, int n) {
        int dimension = columnSpec.length;
        multiples = new int[dimension];
        multiplesSigned = new int[dimension];
        resetSequence(n);

        nColumnsWithFreeSign = -1;
        indexOfColumnsWithFreeSign = new int[dimension];
        this.columnSpec = columnSpec;

        this.nAlreadyUsed = 0;
        this.alreadyUsedLinearCombination = new int[dimension][];
        for (int i = 0; i < dimension; i++) {
            this.alreadyUsedLinearCombination[i] = null;
        }
    }

    
    
    
    
    // This resets the sequence of linear combinations to (n, 0, 0, ...),
    // but all other things, such as registered used linear combinations
    // and the sign mask for columns is not reset.
    final void resetSequence(int n) {
        multiples[0] = n;
        for (int i = 1; i < multiples.length; i++) {
            multiples[i] = 0;
        }
    }

    
    
    
    public int[] getMultiples() {
        return multiples;
    }

    
    
    
    public boolean isUseful() {
        boolean useful = false;

        // A linear combination of only POSITIVE columns does not add
        // anything to the basis of the spanned space. The POSITIVE
        // columns are taken as part of the basis anyway, and a linear
        // combination of them does not add anything new.
        for (int i = 0; i < multiples.length && !useful; i++) {
            if (columnSpec[i] == Recombine.MIXED && multiples[i] != 0) {
                useful = true;
            }
        }

        if (useful) {
            // Another reason for not being useful is that the gcd
            // of all multiples of the MIXED columns is not equal to 1.
            // If a combination A + nM (here A is a linear combination
            // of the POSITIVE columns and M is a linear combination
            // of the mixed columns) forms a vector with non-negative
            // elements only for any n > 1, then A + M also forms a 
            // vector with non-negative elements only. So, it makes
            // no sense to check for linear combinations of the MIXED
            // columns with a gcd of the multiples of the columns,
            // larger than 1.
            int gcd = 0;
            for (int i = 0; i < multiples.length && gcd != 1; i++) {
                if (columnSpec[i] == Recombine.MIXED) {
                    gcd = small_gcd(gcd, multiples[i]);
                }
            }
            if (gcd > 1) {
                useful = false;
            }
        }
        return useful;
    }

    
    
    
    
    // We have n grains and we have to divide them over all
    // positions. The next option is to take one grain from
    // the first non-empty position and move it to the right,
    // while taking all remaining grains and putting these
    // in the position at index 0.
    public int[] next() {
        int idx = 0;
        while (multiples[idx] == 0) {
            idx++;
        }
        if (idx == multiples.length - 1) {
            // The total sum is in the highest element, nothing can be
            // shifted anymore. We return null, indicating that no
            // more possibilities can be generated.
            return null;
        }

        // Take one grain and move that to the next position,
        // while putting all remaining grains at position 0.
        multiples[0] = multiples[idx] - 1;
        if (idx != 0) {
            multiples[idx] = 0;
        }
        multiples[idx + 1]++;

        // Set the number of columns, where the sign of the multiplier can
        // be chosen freely, to -1, indicating that the nextSigned() 
        // method needs to initialize the sign-pattern generator.
        nColumnsWithFreeSign = -1;

        return multiples;
    }

    
    
    
    
    public int[] nextSigned() {
        if (nColumnsWithFreeSign == -1) {
            // We have to do some initialization stuff. We determine
            // how many columns there are for which the sign can be
            // chosen freely, and we determine the index of these columns.
            nColumnsWithFreeSign = 0;
            int totalNrOfUsedColumns = 0;
            for (int i = 0; i < multiples.length; i++) {
                if (multiples[i] > 0) {
                    totalNrOfUsedColumns++;
                    if (columnSpec[i] == Recombine.MIXED) {
                        indexOfColumnsWithFreeSign[nColumnsWithFreeSign] = i;
                        nColumnsWithFreeSign++;
                    }
                }
            }

            // If the number of columns with free sign equals the
            // total number of columns, used for this sequence,
            // then we fix the sign of the last column to positive.
            // It is of no use to check all linear combinations
            // for all possible signs. Checking e.g. v1-v2+v3 and
            // -v1+v2-v3 is not useful. The one is simply the
            // negation of the other. This saves half the computation
            // time.
            if (totalNrOfUsedColumns == nColumnsWithFreeSign) {
                nColumnsWithFreeSign--;
            }

            signMask = 1 << nColumnsWithFreeSign;
        }

        // At this place we have done the initialization. Now we can
        // get signed sequences, based on the bits of a number.
        if (signMask > 0) {
            signMask--;
            for (int i = 0; i < multiples.length; i++) {
                multiplesSigned[i] = multiples[i];
            }
            int bit = 1;
            for (int i = 0; i < nColumnsWithFreeSign; i++) {
                if ((bit & signMask) != 0) {
                    multiplesSigned[indexOfColumnsWithFreeSign[i]] *= -1;
                }
                bit <<= 1;
            }
        } 
        else {
            // We are at the end of the list of possible signed
            // combinations for this set of sequences.
            return null;
        }

        if (nAlreadyUsed == 0) {
            // We do not yet have any linear combinations, so
            // this one is always linearly independent.
            return multiplesSigned;
        }

        if (nAlreadyUsed == 1) {
            // We use a special situation in which we simply
            // determine whether the already known linear
            // combination and the new one are independent.
            boolean isIndependent = indep_2vectors(alreadyUsedLinearCombination[0], multiplesSigned);
            return isIndependent ? multiplesSigned : nextSigned();
        }

        // We handle the case of dimension 3 in a special MUCH more lighhtweight way.
        // This point only is reached for a situation in which we already have two
        // independent vectors and a third one is added. The other situations are
        // handled in the code above.
        if (multiplesSigned.length == 3) {
            // We already have two already known linear combinations. We
            // compute the determinant of the matrix, made up of these
            // two linear combinations and the newly determined one. If
            // this determinant equals 0, then the newly determined one
            // is not linearly independent.
            int u[] = alreadyUsedLinearCombination[0];
            int v[] = alreadyUsedLinearCombination[1];
            int w[] = multiplesSigned;
            long det = (long) u[0] * (long) v[1] * (long) w[2];
            det += (long) v[0] * (long) w[1] * (long) u[2];
            det += (long) w[0] * (long) u[1] * (long) v[2];
            det -= (long) u[2] * (long) v[1] * (long) w[0];
            det -= (long) v[2] * (long) w[1] * (long) u[0];
            det -= (long) w[2] * (long) u[1] * (long) v[0];
            return (det == 0) ? nextSigned() : multiplesSigned;
        }

        // Gooogle: linear subspace basis positive orthant
        // The new linear combination of base vectors must be linearly independent
        // of the already known ones. We check the rank of the matrix, built up by
        // the linear combinations we already have and the newly determined one.
        Matrix M = new Matrix(nAlreadyUsed + 1, multiplesSigned.length);
        for (int r = 0; r < nAlreadyUsed; r++) {
            M.setRow(r, alreadyUsedLinearCombination[r]);
        }
        M.setRow(nAlreadyUsed, multiplesSigned);

        // If we have a combination, which does not add anything new
        // to the base, then we return the next signed combination,
        // otherwise we return the current one.
        return (M.rank() <= nAlreadyUsed) ? nextSigned() : multiplesSigned;
    }

    
    
    
    private static boolean indep_2vectors(int[] v, int[] w) {
        int n = v.length;

        for (int c = 0; c < n; c++) {
            if (v[c] == 0) {
                if (w[c] == 0) {
                    // Both elements are zero. Such an element cannot be
                    // used for testing and we advance to the next one.
                    continue;
                }

                return true;
                // They are independent.
            }

            if (w[c] == 0) {
                return true;
                // They are independent.
            }

            /* This code is not needed here, we simply use w[c]
               and v[c], which saves computation time.
                  int gcd = small_gcd(v[c], w[c]);
                  long fac0 = w[c] / gcd;
                  long fac1 = v[c] / gcd;
            */
            long fac0 = w[c];
            long fac1 = v[c];

            for (int cc = c + 1; cc < n; cc++) {
                if (v[cc] * fac0 - w[cc] * fac1 != 0) {
                    return true;
                }
            }
            return false;
        }

        return false;
    }

    
    
    
    public void registerAsUsed(int[] arr) {
        if (arr.length == multiples.length && nAlreadyUsed < arr.length) {
            alreadyUsedLinearCombination[nAlreadyUsed] = (int[]) arr.clone();
            nAlreadyUsed++;
        }
        else {
            throw new RuntimeException("Invalid use of sequencer, registering invalid linear combination");
        }
    }

    
    
    
    public int getNAlreadyUsed() {
        return nAlreadyUsed;
    }

    
    
    
    public int[][] getAlreadyUsedLinearCombinations() {
        return alreadyUsedLinearCombination;
    }

    
    
    
    static private int small_gcd(int x, int y) {
        if (x == 0) {
            return y;
        }

        for (;;) {
            int z = y % x;
            if (z == 0) {
                break;
            }
            y = x;
            x = z;
        }

        return x;
    }
}





public class Sequencer {

    private int n;
    private int currentN;
    private SequencerForOneN sequencer;
    private boolean hasUsefulSeq;

    public Sequencer(int[] columnSpec, int n) {
        this.n = n;
        currentN = 2;
        sequencer = (n >= currentN) ? new SequencerForOneN(columnSpec, currentN) : null;
        hasUsefulSeq = false;
    }

    
    
    
    public int[] next() {
        if (sequencer == null) {
            return null;
        }

        while (!hasUsefulSeq) {
            // Obtain the next sequence. If the current sequencer is at its
            // end, then reset the sequencer for the next currentN.
            int seq[] = sequencer.next();
            if (seq == null) {
                if (currentN < n) {
                    currentN++;
                    sequencer.resetSequence(currentN);
                    seq = sequencer.next();
                } else {
                    sequencer = null;
                }
            }

            // If we have no sequencer at this point, then we are at
            // the end of the total sequence for the maximum number n.
            // In that case we bail out of the loop.
            if (sequencer == null) {
                break;
            }

            // If the sequence we found is a useful one, then we
            // set the hasUsefulSeq flag to true, which causes us
            // to break out of the loop.
            if (sequencer.isUseful()) {
                hasUsefulSeq = true;
            }
        }

        int signedSeq[] = null;
        if (hasUsefulSeq) {
            signedSeq = sequencer.nextSigned();

            // If we are at the last signedSeq, which will be given for this
            // useful sequence, then we set the signedSeq flag to false again.
            hasUsefulSeq = (signedSeq != null);

            // If we don't have a useful sequence anymore, then 
            // we already set hasUsefulSeq to false again, and we
            // call this method another time recursively.
            if (!hasUsefulSeq) {
                signedSeq = next();
            }
        }

        return signedSeq;
    }

    
    
    
    
    public void registerAsUsed(int[] usedLinearCombination) {
        if (sequencer != null) {
            sequencer.registerAsUsed(usedLinearCombination);
        }
    }

    
    
    
    
    public static void main(String[] args) {
        int[] columnSpec = new int[]{Recombine.MIXED, Recombine.POSITIVE, Recombine.POSITIVE, Recombine.MIXED};
        int N = 10;
        Sequencer s = new Sequencer(columnSpec, N);
        s.registerAsUsed(new int[]{2, 0, 0, 1});
        s.registerAsUsed(new int[]{2, 1, 0, -1});
        int seq[];
        int nSeq = 0;
        while ((seq = s.next()) != null) {
            for (int i = 0; i < columnSpec.length; i++) {
                System.out.print(" " + seq[i]);
            }
            System.out.println("");
            nSeq++;
        }
        System.out.println("Number of sequences: " + nSeq);
    }
}

package net.oelen.chemeq.math;

import java.math.BigInteger;

// A simple class for arithmetic on rational numbers.
// This is not a very efficient class and it is very
// simple, but it is OK for the light use in this
// chemstry equation solver.
public class Rational {

    static public final Rational ZERO = new Rational();
    static public final Rational ONE = new Rational(1);

    private BigInteger den;
    private BigInteger num;

    
    
    public Rational() {
        num = BigInteger.ZERO;
        den = BigInteger.ONE;
    }

    
    
    public Rational(BigInteger num, BigInteger den) {
        this.num = num;
        this.den = den;
        canonical();
    }

    
    
    public Rational(BigInteger num) {
        this.num = num;
        this.den = BigInteger.ONE;
    }

    
    
    public Rational(long num, long den) {
        this.num = BigInteger.valueOf(num);
        this.den = BigInteger.valueOf(den);
        canonical();
    }

    
    
    public Rational(long num) {
        this.num = BigInteger.valueOf(num);
        this.den = BigInteger.ONE;
    }

    
    
    private void canonical() {
        if (den.equals(BigInteger.ZERO)) {
            throw new ArithmeticException("Zero denominator in Rational number");
        }

        if (num.equals(BigInteger.ZERO)) {
            num = BigInteger.ZERO;
            den = BigInteger.ONE;
            return;
        }

        BigInteger gcd = den.gcd(num);
        num = num.divide(gcd);
        den = den.divide(gcd);

        if (den.signum() == -1) {
            num = num.negate();
            den = den.negate();
        }
    }

    
    
    public BigInteger getNum() {
        return num;
    }

    
    
    public BigInteger getDen() {
        return den;
    }

    
    
    public Rational add(Rational rat) {
        BigInteger n1 = this.num.multiply(rat.den);
        BigInteger n2 = this.den.multiply(rat.num);
        BigInteger newnum = n1.add(n2);
        BigInteger newden = this.den.multiply(rat.den);
        return new Rational(newnum, newden);
    }

    
    
    public Rational sub(Rational rat) {
        BigInteger n1 = this.num.multiply(rat.den);
        BigInteger n2 = this.den.multiply(rat.num);
        BigInteger newnum = n1.subtract(n2);
        BigInteger newden = this.den.multiply(rat.den);
        return new Rational(newnum, newden);
    }

    
    
    public Rational mul(Rational rat) {
        BigInteger newnum = this.num.multiply(rat.num);
        BigInteger newden = this.den.multiply(rat.den);
        return new Rational(newnum, newden);
    }

    
    
    public Rational div(Rational rat) {
        BigInteger newnum = this.num.multiply(rat.den);
        BigInteger newden = this.den.multiply(rat.num);
        return new Rational(newnum, newden);
    }

    
    
    public Rational minus() {
        return new Rational(num.negate(), den);
    }

    
    /*
	 * Determines the lcm of the argument n and the 
	 * denominator of the rational number. This
	 * method is useful for determining the lcm
	 * of a set of rational numbers by calling it
	 * for each rational number, passing the output
	 * of one call to the argument of the next call.
     */
    public BigInteger denLcm(BigInteger n) {
        BigInteger gcd = den.gcd(n);
        return n.divide(gcd).multiply(den);
    }

    
    
    public boolean isOne() {
        return den.equals(num);
    }

    
    
    public int sign() {
        return num.signum();
    }
}

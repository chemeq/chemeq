package net.oelen.chemeq.basics;

// This is a factory class, which allows one to obtain
// elements based on number or symbol.
public class ElementFactory {

    static final int MAX_NR_OF_PSEUDOS = 100;
    final private Element m_PseudoElements[] = new Element[MAX_NR_OF_PSEUDOS];
    private int m_NrOfPseudos = 0;

    
    
    // Use Flyweight-pattern for elements with atomic number
    // less than or equal to NR_OF_REAL_ELEMENTS. These elements
    // are the same for all situations and never will be
    // modified. No need to have duplicates of them.
    public Element getElementByNumber(int number) {
        if (number <= 0) {
            return null;
        }

        if (number <= IsotopeData.NR_OF_REAL_ELEMENTS) {
            return Isotopes.elems[number][0];
        }

        number -= IsotopeData.NR_OF_REAL_ELEMENTS + 1;
        if (number < m_NrOfPseudos) {
            return m_PseudoElements[number];
        }

        return null;
    }

    
    
    // This obtains a specific isotope of an element.
    // The first argument is the number of protons in the
    // nucleus, the second argument is the number of protons
    // plus the number of neutrons of the specific isotope.
    // If the isotope does not exist (i.e. it is not present
    // in the NIST element data, then null is returned).
    public Element getElementByNumber(int number, int isotope) {
        if (number <= 0) {
            return null;
        }

        if (number <= IsotopeData.NR_OF_REAL_ELEMENTS) {
            Element elem = null;
            Element[] elems = Isotopes.elems[number];
            for (int j = 1; elem == null && j < elems.length; j++) {
                if (elems[j].isotope == isotope) {
                    elem = elems[j];
                }
            }
            return elem;
        }

        return null;
    }

    
    
    
    // Obtains an element, based on the symbol for the
    // element. Symbols H, He, Li, Be and so on are supported.
    // One can add additional elements (pseudo/fantasy elements)
    // and once added, these can be retrieved by means of their
    // symbol.
    public Element getElementBySymbol(String symbol) {

        if (symbol == null || "".equals(symbol)
            || !Character.isUpperCase(symbol.charAt(0))) {
            return null;
        }

        Element elem = null;

        // First look up the array of real elements.
        for (int i = 1; i <= IsotopeData.NR_OF_REAL_ELEMENTS; i++) {
            Element el = Isotopes.elems[i][0];
            if (el.sym.equals(symbol)) {
                elem = el;
                break;
            }
        }

        // If no element was found, then look up the
        // array of pseudo elements.
        if (elem == null) {
            for (int i = 0; i < m_NrOfPseudos; i++) {
                Element el = m_PseudoElements[i];
                if (el.sym.equals(symbol)) {
                    elem = el;
                    break;
                }
            }
        }

        return elem;
    }

    
    
    
    // Retrieves a specific isotope of one of the real elements.
    // The first argument is the symbol, i.e. H, He, Li, Be, B and
    // so on, the second argument is the isotope, identified by its
    // number of protons + number of neutrons in the nucleus.
    public Element getElementBySymbol(String symbol, int isotope) {

        if (symbol == null || "".equals(symbol)
            || !Character.isUpperCase(symbol.charAt(0))) {
            return null;
        }

        Element elem = null;

        // First look up the array of real elements.
        for (int i = 1; elem == null && i <= IsotopeData.NR_OF_REAL_ELEMENTS; i++) {
            Element[] elems = Isotopes.elems[i];
            if (elems[0].sym.equals(symbol)) {
                for (int j = 1; elem == null && j < elems.length; j++) {
                    if (elems[j].isotope == isotope) {
                        elem = elems[j];
                    }
                }
            }
        }

        return elem;
    }

    
    
    
    
    // Adds a pseudo element to the table of elements and
    // returns the element just added. If no element can
    // be added, a null element is returned.
    // The first argument is a string, being the symbol of
    // the newly created element. The second argument is 
    // the atomic mass of the newly added element. The new
    // elements cannot be retrieved by isotope, they get
    // isotope number 0. They can only be retrieved on the
    // basis of the symbol.
    public Element addPseudoElement(String symbol, double m) {
        if (m_NrOfPseudos == MAX_NR_OF_PSEUDOS) {
            return null;
        }

        // We do not allow a symbol, which is not a 
        // valid element symbol. An element symbol
        // must start with an upper case character,
        // followed by at most two lowercase, *
        // or ' characters.
        if (!isValidElementSymbol(symbol)) {
            return null;
        }

        // If an element exists with this symbol already, then
        // return the value null. We cannot replace elements.
        if (getElementBySymbol(symbol) != null) {
            return null;
        }

        Element newElem = new Element();
        m_PseudoElements[m_NrOfPseudos] = newElem;
        m_PseudoElements[m_NrOfPseudos].sym = symbol;
        m_PseudoElements[m_NrOfPseudos].mass = m;
        m_PseudoElements[m_NrOfPseudos].isotope = 0;
        m_PseudoElements[m_NrOfPseudos].num = IsotopeData.NR_OF_REAL_ELEMENTS + m_NrOfPseudos + 1;
        m_NrOfPseudos++;

        return newElem;
    }

    
    
    
    // Removes all pseudo elements, added during the execution
    // of the program.
    public void clearPseudoElements() {
        m_NrOfPseudos = 0;

        // Help the garbage collector somewhat.
        for (int i = 0; i < MAX_NR_OF_PSEUDOS; i++) {
            m_PseudoElements[i] = null;
        }
    }

    
    
    
    
    // Computes the total molar mass of a molecule. A molecule is
    // represented by an array of integers, where the position in
    // the array determines the element and the value at that 
    // position determines the number of atoms of that element.
    //
    // Example:
    // We have the molecule H2SO4. This contains two H-atoms (Z = 1),
    // 4 O-atoms (Z = 8) and 1 S atom (Z = 16). For this molecule
    // the input array must be:
    //     H  He Li Be B  C  N  O  F  Ne Na Mg Al Si P  S  Cl ....
    // [0, 2, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, ....]
    // The element S is the element with largest Z. The array may
    // contain more elements, but these must be equal to 0.
    //
    // It also is possible to have pseudo/fantasy elements in the
    // array. These must appear at index 110 and higher, after the
    // real elements up to and including Mt (which has Z = 109).
    public double totalMass(int elemArr[]) {
        int size = elemArr.length;
        double totalM = -IsotopeData.ELECTRON_MASS * elemArr[0];

        int i = 1;
        for (int ei = 1; ei <= IsotopeData.NR_OF_REAL_ELEMENTS && i < size; i++, ei++) {
            totalM += Isotopes.elems[ei][0].mass * elemArr[i];
        }
        for (int ei = 0; ei < m_NrOfPseudos && i < size; i++, ei++) {
            totalM += m_PseudoElements[ei].mass * elemArr[i];
        }

        return totalM;
    }

    
    
    
    
    // Returns an array of integers, which contains space for all real
    // elements plus the number of defined pseudo elements up to now.
    public int[] newElemCountArray() {
        int[] arr = new int[1 + IsotopeData.NR_OF_REAL_ELEMENTS + m_NrOfPseudos];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = 0;
        }
        return arr;
    }

    
    
    
    // Checks whether a given string could be used as symbol for an
    // element. This can be useful for validating user input in a
    // program, which allows users to create new fantasy elements
    // or symbols for specific isotopes of existing elements. Such
    // a string must satisfy certain rules.
    public static boolean isValidElementSymbol(String sym) {
        if (sym == null) {
            return false;
        }

        switch (sym.length()) {
            case 3:
                if (!(Character.isLowerCase(sym.charAt(2))
                    || sym.charAt(2) == '*' || sym.charAt(2) == '\'')) {
                    return false;
                }
            // Fall through intentionally
            case 2:
                if (!(Character.isLowerCase(sym.charAt(1))
                    || sym.charAt(1) == '*' || sym.charAt(1) == '\'')) {
                    return false;
                }
            // Fall through intentionally
            case 1:
                if (!Character.isUpperCase(sym.charAt(0))) {
                    return false;
                }
                break;
            default:
                return false;
        }

        return true;
    }
}

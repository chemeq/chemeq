package net.oelen.chemeq.basics;



// This is a class, wich represents chemical elements.
// This is used to represent all chemical elements in their
// natural average distribution of isotopes, but this also
// can be used to represent specific isotopes, and even
// fantasy elements can be represented. The latter can be
// created dynamically.
public class Element {
	// Only accessible by code in same package.
	String sym;     // The symbol of the element, H, He, Li, Be, etc.
	double mass;    // The atomic mass of the element. For the normal
                        // elements this is the average natural atomic
                        // weight. For specific isotopes, this is the
                        // atomic weight for the specific isotope.
	int num;        // The number of protons (Z) in the nucleus.
	int isotope;    // The number protons + the number of neutrons in
                        // the nucleus for specific isotopes, the value 0
                        // is used for the naturally occurring mixes of
                        // isotopes and for fantasy elements.

        
        
	// Public getters. No setters are provided,
	// elements are not set directly, but info
	// is derived.
	public String getSym() {return sym;}
	public double getMass() {return mass;}
	public int getNum() {return num;}
	public int getIsotope() {return isotope;}
}

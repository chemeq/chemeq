package net.oelen.chemeq.basics;




/* A formula parser.
 * Chemical formulae can be described as follows:
 * Something between [] can occur zero or more times.
 * Something between {} can occur zero or one time.
 *
 * formula        ::= <topformula> | "e"
 * topformula     ::= <simple_formula> ["." <simple_formula>]
 * simple_formula ::= [<digit>] <factor> [<factor>]
 * factor         ::= <element_count>
 *                         |    "(" <topformula> ")" [<digit>]
 *                         |    "[" <topformula> "]" [<digit>]
 * element_count  ::= (<element> | "+" | "-") [<digit>]
 * digit          ::= "0" | "1"  ... "9"
 * element        ::= "H" | "He" | "Li" .... "Mt" | <pseudoelement>
 * pseudoelement  ::= <uppercase> {<lowercase>} {<lowercase>}
 * uppercase      ::= "A" | "B" | ... "Z"
 * lowercase      ::= "'" | "*" | "a" | "b" | .... "z"
 *
 */


public class Formula {

    final private Scanner m_Scanner;
    final private ElementFactory elemFact;

    
    
    // Create a new formula object and attach an element factory
    // to it. The formula may contain any of the real (standard)
    // elements, and it may contain additional pseudo/fantasy
    // elements, but only if these are present in the associated
    // element factory.
    // The first argument is a string, representing the formula,
    // e.g. "H2SO4" for sulfuric acid, or "NO3(-)" for the nitrate
    // ion. Also more complicated formulae like "K4Fe(CN)6.3H2O"
    // are allowed.
    public Formula(String frm, ElementFactory elemFact) {
        m_Scanner = new Scanner(frm);
        this.elemFact = elemFact;
    }

    
    
    
    // Parse the formula, based on the string, which was passed
    // on creation of the formula. The value true is returned if
    // parsing succeeds, the value false is returned otherwise.
    // In the array totalElemCount for each of the types of
    // atoms in the formula, the count is increased accordingly.
    // The count is not reset, the count, already present in the
    // array is incremented. As an example, we use the formula
    // for "K4Fe(CN)6.3H2O" (yellow prussiate of potash). After
    // parsing, the counts in the array are incremented as
    // follows:
    //    Position  1, H  : +6
    //    Position  6, C  : +6
    //    Position  7, N  : +6
    //    Position  8, O  : +3
    //    Position 19, K  : +4
    //    Position 26, Fe : +1
    // The supplied array must have sufficient length to hold
    // counts for all elements in the forumula. If pseudo
    // elements are used, then the count continues from indexes
    // 110 and upwards. 
    // By parsing multiple formulae one can count all atoms in
    // all formulae combined.
    // The formulae may also contain charges. The total charge
    // is counted at position 0. This count may also be negative.
    // E.g. in "SO4(2-)", at position 0, we have a decrement of 2.
    //
    // This is the only public method in this class. All methods
    // below are part of the parser and all are private methods.
    public boolean parse(int[] totalElemCount) {

        m_Scanner.pushMark();

        String s = m_Scanner.getSymbol();
        if (s == null) {
            m_Scanner.popMark(1);
            return false;
        }

        // We allow a single "e", but no other characters.
        if ("e".equals(s)) {
            totalElemCount[0]--;
            s = m_Scanner.getSymbol();
            m_Scanner.popMark(1);
            return (s == null);
        }

        // We do not allow single "-" or "+" characters.
        if ("+".equals(s) || "-".equals(s)) {
            m_Scanner.popMark(1);
            return false;
        }

        m_Scanner.popMark(1);

        m_Scanner.pushMark();
        boolean parseResult = topformulaParse(totalElemCount);
        s = m_Scanner.getSymbol();
        m_Scanner.popMark(1);

        // We must have a valid formula and no trailing garbage.
        return parseResult && (s == null);
    }

    
    
    
    private boolean topformulaParse(int[] totalElemCount) {
        // We expect at least one simple formula. After the
        // first simple formula, dots and additional simple
        // formulae may follow. If we find a dot, we expect
        // another simple formula.
        boolean result = simpleformulaParse(totalElemCount);
        while (result) {
            // Get next symbol. If it is a dot, we continue
            // parsing another simple formula.
            m_Scanner.pushMark();
            String s = m_Scanner.getSymbol();
            if (".".equals(s)) {
                result = simpleformulaParse(totalElemCount);
                m_Scanner.undoMark(1);
            } 
            else {
                // We did not have a dot. We either are completely
                // at the end, or we have a topformula, nested in
                // [] or (). In both cases we bail out of the loop.
                m_Scanner.popMark(1);
                break;
            }
        }

        return result;
    }

    
    
    
    
    private boolean simpleformulaParse(int[] totalElemCount) {
        // Allocate a local array of element counts. We need
        // this, because simple formulae may contain a multiplier
        // not equal to 1.
        int[] localElemCount = elemFact.newElemCountArray();

        // A simple formula may start with a number, an example is
        // 2H2O. All atoms in the preceeding part of the formula
        // then will be multiplied by the number. The example has
        // 4 H-atoms and 2 O-atoms.
        int multiplier;
        m_Scanner.pushMark();
        String s = m_Scanner.getSymbol();
        if (IS_NUMBER(s)) {
            multiplier = Integer.parseInt(s);
            m_Scanner.undoMark(1);
        } 
        else {
            multiplier = 1;
            m_Scanner.popMark(1);
        }

        // From here, we parse factors. We count
        // the number of factors we parsed.
        boolean result = true;
        int nFactors = 0;
        while (result) {
            m_Scanner.pushMark();
            s = m_Scanner.getSymbol();
            m_Scanner.popMark(1);
            if ("[".equals(s) || "(".equals(s) || "+".equals(s) || "-".equals(s) || IS_ELEMENT(s)) {
                result = factorParse(localElemCount);
                nFactors++;
            } 
            else {
                break;
            }
        }

        // Update the total element count, based on
        // all factors and multiplied by the number
        // in front of all factors.
        for (int i = 0; i < localElemCount.length; i++) {
            totalElemCount[i] += localElemCount[i] * multiplier;
        }

        return result && (nFactors > 0);
    }

    
    
    
    
    
    private boolean factorParse(int[] totalElemCount) {

        boolean result;
        String s = m_Scanner.getSymbol();
        if ("[".equals(s) || "(".equals(s)) {
            // Allocate a local array of element counts. We need
            // this, because simple formulae may contain a multiplier
            // not equal to 1. Here, the multiplier is the number
            // after he factor. E.g. in Fe(OH)3, there are is a
            // multiplier 3 for (OH).
            int[] localElemCount = elemFact.newElemCountArray();

            // We have a factor, which contains a topformula.
            // An example is CH(CH3)3, where one of the factors
            // is (CH3).
            // When the topformula is parsed, then we expect a
            // matching closing bracket. Which symbol is expected
            // depends on the opening bracked.
            String expectedClosingBracket = ("[".equals(s)) ? "]" : ")";

            // Parse the topformula.
            result = topformulaParse(localElemCount);

            s = m_Scanner.getSymbol();
            result = result && (expectedClosingBracket.equals(s));

            if (result) {
                m_Scanner.pushMark();
                s = m_Scanner.getSymbol();
                int multiplier;
                if (IS_NUMBER(s)) {
                    multiplier = Integer.parseInt(s);
                    m_Scanner.undoMark(1);
                } 
                else {
                    multiplier = 1;
                    m_Scanner.popMark(1);
                }

                // Update the total element count, based on
                // all factors and multplied by the number
                // in front of all factors.
                for (int i = 0; i < localElemCount.length; i++) {
                    totalElemCount[i] += localElemCount[i] * multiplier;
                }
            }
        } 
        else {
            // We have a factor, consisting of a simple element_count
            // element with an element, a "+" or a "-" and an optional
            // multiplier. We handle this situation here, no explicit
            // call to a parser for an element_count.
            int elemIndex;
            int elemSign;
            if ("+".equals(s)) {
                // positive charge
                elemIndex = 0;
                elemSign = 1;
                result = true;
            } 
            else if ("-".equals(s)) {
                // negative charge
                elemIndex = 0;
                elemSign = -1;
                result = true;
            } 
            else {
                Element elem = elemFact.getElementBySymbol(s);
                if (elem != null) {
                    // One of the true elements or a pseudoelement
                    elemIndex = elem.getNum();
                    elemSign = 1;
                    result = true;
                } 
                else {
                    // Element is not found, generate an error.
                    elemIndex = -1;
                    elemSign = 0;
                    result = false;
                }
            }

            if (result) {
                m_Scanner.pushMark();
                s = m_Scanner.getSymbol();
                int multiplier;
                if (IS_NUMBER(s)) {
                    multiplier = Integer.parseInt(s);
                    m_Scanner.undoMark(1);
                } 
                else {
                    multiplier = 1;
                    m_Scanner.popMark(1);
                }

                totalElemCount[elemIndex] += elemSign * multiplier;
            }
        }
        return result;
    }

    
    
    
    
    // Little helper method, which determined whether a string is
    // an element, pseudo element or a charge sign. All of these
    // are handled as elements by the formula parser.
    private static boolean IS_ELEMENT(String s) {
        if (s == null) {
            return false;
        }

        return (Character.isUpperCase(s.charAt(0)) || "+".equals(s) || "-".equals(s));
    }

    
    
    
    
    // Little helper method, which determines whether a string is
    // a number. Checking the first character is sufficient. If
    // this is a digit, then all will be digits. The scanner assures
    // that this is true.
    private static boolean IS_NUMBER(String s) {
        if (s == null) {
            return false;
        }
        return Character.isDigit(s.charAt(0));
    }
    
    
    
    

    // This main() method shows how to use the formula parser.
    
    /*
    public static void main(String[] args) {
        String formula = "H2SO4";
        ElementFactory fact = new ElementFactory();
        int elemCount[] = fact.newElemCountArray();
        Formula form = new Formula(formula, fact);
        boolean result = form.parse(elemCount);
        if (result) {
            if (elemCount[0] != 0) {
                System.out.println("Charge: " + elemCount[0]);
            }
            for (int i = 1; i < elemCount.length; i++) {
                if (elemCount[i] != 0) {
                    Element elem = fact.getElementByNumber(i);
                    System.out.println(elem.sym + " : " + elemCount[i]);
                }
            }
            System.out.println("Total mass: " + fact.totalMass(elemCount));
        } 
        else {
            System.out.println("Invalid formula " + formula);
        }
    }
    */
}





/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */





// A helper class to scan strings and extract symbols from the
// string. There is just lexical scanning in this class. Giving
// meaning to the symbols is done in the parser above.
class Scanner {

    static final private int MAX_MARKS = 10;
    private int m_Pos;
    final private char m_Char[];
    final private int m_Marks[];
    int m_MarkSP;

    
    public Scanner(String s) {
        m_Pos = 0;
        m_Char = s.toCharArray();
        m_Marks = new int[MAX_MARKS];
        m_MarkSP = 0;
    }

    
    
    
    private void findNextPos() throws ArrayIndexOutOfBoundsException {
        while (m_Char[m_Pos] <= ' ') {
            m_Pos++;
        }
    }

    
    
    
    private String getElementSymbol() {
        char elem[] = new char[3];
        int nChars = 1;
        elem[0] = m_Char[m_Pos];
        m_Pos++;

        while (m_Pos < m_Char.length
            && (Character.isLowerCase(m_Char[m_Pos]) || m_Char[m_Pos] == '*' || m_Char[m_Pos] == '\'')
            && nChars < 3) {
            elem[nChars] = m_Char[m_Pos];
            nChars++;
            m_Pos++;
        }

        return String.copyValueOf(elem, 0, nChars);
    }

    
    
    
    
    private String getNumberSymbol() {
        char num[] = new char[9];
        int nChars = 1;
        num[0] = m_Char[m_Pos];
        m_Pos++;

        while (m_Pos < m_Char.length
            && Character.isDigit(m_Char[m_Pos])
            && nChars < 9) {
            num[nChars] = m_Char[m_Pos];
            nChars++;
            m_Pos++;
        }

        return String.copyValueOf(num, 0, nChars);
    }

    
    
    
    
    public String getSymbol() {
        try {
            findNextPos();

            if (Character.isUpperCase(m_Char[m_Pos])) {
                return getElementSymbol();
            }

            if (Character.isDigit(m_Char[m_Pos])) {
                return getNumberSymbol();
            }

            String sym = String.valueOf(m_Char[m_Pos]);
            m_Pos++;
            return sym;
        } 
        catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }

    
    
    
    public void pushMark() {
        if (m_MarkSP >= MAX_MARKS) {
            throw new RuntimeException("chem.basics.Scanner: too many marker pushes");
        }
        m_Marks[m_MarkSP] = m_Pos;
        m_MarkSP++;
    }

    
    
    
    
    public void popMark(int steps) {
        m_MarkSP -= steps;
        if (m_MarkSP < 0) {
            throw new RuntimeException("chem.basics.Scanner: too many marker pops");
        }
        m_Pos = m_Marks[m_MarkSP];
    }

    
    
    
    
    public void undoMark(int steps) {
        m_MarkSP -= steps;
        if (m_MarkSP < 0) {
            throw new RuntimeException("chem.basics.Scanner: too many marker undo's");
        }
    }
}

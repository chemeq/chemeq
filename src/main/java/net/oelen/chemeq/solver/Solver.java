package net.oelen.chemeq.solver;

import net.oelen.chemeq.basics.Element;
import net.oelen.chemeq.basics.ElementFactory;
import net.oelen.chemeq.basics.Formula;
import net.oelen.chemeq.math.Matrix;
import net.oelen.chemeq.math.Recombine;

// This is a class, containing the solver of a chemical equation.
// Solving is done by computing the null-space of a matrix. See
// below for a detailed explanation of the used algorithm.
public class Solver {

    final private String[] in;
    final private String[] out;
    private String errorMsg;

    private Matrix A;
    private Matrix N;

    ElementFactory elemFact = null;

    public Solver(ElementFactory elemFact, String[] in, String[] out) {
        this.in = in;
        this.out = out;
        A = null;
        N = null;
        this.elemFact = elemFact;
        errorMsg = null;
    }

    
    
    
    // Builds up an equation from the list of reactants and the
    // list of products. Reactans and products can be molecules,
    // charges or ions (charged molecules). A reaction is given
    // by a list of reactants and products. An example is the
    // idealized reaction of copper metal with concentrated hot
    // nitric acid in which copper nitrate, water, and nitrogen
    // dioxide are formed. Below follows the list of reactants
    // and products:
    //
    //    Cu, HNO3   --->    Cu(2+), NO2, H2O, NO3(-)
    //
    // From this information an equation is formed of the form
    //   Ax = 0,
    // where A is a matrix and x is the coefficient vector. For
    // the reaction above we have:
    //
    //    a Cu + b HNO3 ---> c Cu(2+) + d NO3(-) + e NO2 + f H2O
    //
    // For each of the elements and for the charges, the left and
    // right number of occurrences must be equal, hence
    //
    //    charge :  0 = 2c - d
    //    Cu     :  a = c
    //    H      :  b = 2f
    //    N      :  b = d + e
    //    O      : 3b = 3d + 2e + f
    //
    // This is reformulated as a matrix equation, by bringing the
    // right part of the equation to the left and writing it as a
    // matrix:
    //
    //      / 0   0  -2   1   0   0 \ /a\
    //      | 1   0  -1   0   0   0 | |b|
    //      | 0   1   0   0   0  -2 | |c| = 0
    //      | 0   1   0  -1  -1   0 | |d|
    //      \ 0   3   0  -3  -2  -1 / |e|
    //                                \f/
    //
    // This equation has a one-dimensional solution space, the so-
    // called null-space of the matrix. Normally, the null-space
    // is one-dimensional, which means that there is a strict
    // ratio of all reactants and products and the only freedom
    // is a scaling factor for the balanced equation. Sometimes,
    // however, the null-space has dimension, larger than 1. In
    // such cases, there is no unique way of balancing the equation,
    // but still, the solution obeys certain rules. More info on
    // that is given in the accompanying documentation of the web-
    // program.
    public boolean buildEquation() {
        int elemCountIn[] = elemFact.newElemCountArray();
        int elemCountOut[] = elemFact.newElemCountArray();
        int nElements;
        boolean hasChargeIn, hasChargeOut;
        int chargeSignIn, chargeSignOut;
        int matrixIndex[] = elemFact.newElemCountArray();
        boolean result = true;

        // We parse all reactant formulas and all product formulas.
        // For all elements we do the parsing cumulatively, we simply
        // add all elements we find in the list of reactants and we
        // add all elements we find in the list of products. Using
        // this simple algorithm, we easily find all elements, occuring
        // at the left side and occurring at the right side. A non-
        // zero entry for the element means that it is present.
        // We perform this algorithm independenly for the reactants
        // and for the products.
        // For charges, things are much more involved, and this is
        // because charges can be both positive and negative. We
        // cannot simply do cumulative parsing of charges. Suppose
        // that we would have Na(+), H(+) and SO4(2-) in the set
        // of products then the total charge after cumulative
        // parsing of all formulas would be equal to 0 and we would
        // falsely conclude there is no charge.
        // For charges a more involved algortihm is used. Two variables
        // are tracked during parsing, a boolean which tells there is
        // a charge, and an integer, which tells the sign of the charge.
        // This is done again for reactants and products, so in total
        // there are 4 of such tracking variables.
        // The algorithm is as follows:
        // - Initially set the boolean hasCharge to false and set
        //   the sign of the charge to 0.
        // - Before parsing a single reactant or product, reset the
        //   elemCount[0] element to 0.
        // - Parse the formula and see if there is a charge (the 
        //   elemCount[0] value is not equal to 0).
        // - If there is a charge, then check the current value of the
        //   hasCharge boolean.
        //      - If this still is false, then set the sign of the charge
        //        to the sign of the elemCount[0] element.
        //      - If this is true, then check whether the currently known
        //        sign is the same as the sign in the elemCount[0] element.
        //        If they are the same, then don't change the sign,
        //        otherwise set the sign to 0. This assures that once there
        //        is a charge, and the sign is set to 0, it remains 0.
        //
        // At the end of this algorithm, we are interested in the 
        // sign of the charge. If it equals 0, then there either is no
        // charge at all in any of the reactants (or products), or there
        // are charges, but they are of opposite sign. If it is not equal
        // to 0, then there are charges and they all have the same sign.
        // Parse all reactant formulas and keep track
        // of all elements (we do not reset elemCountIn
        // between parses of individual formulas).
        hasChargeIn = false;
        chargeSignIn = 0;
        for (String compound : in) {
            Formula form = new Formula(compound, elemFact);
            elemCountIn[0] = 0;
            result = form.parse(elemCountIn);
            if (!result) {
                errorMsg = "Invalid formula for reactant: " + compound;
                break;
            }
            if (elemCountIn[0] != 0) {
                int curSign = (elemCountIn[0] < 0) ? -1 : 1;
                chargeSignIn = hasChargeIn ? ((curSign != chargeSignIn) ? 0 : curSign) : curSign;
                hasChargeIn = true;
            }
        }
        if (!result) {
            return false;
        }

        // Parse all product formulas and keep track
        // of all elements (we do not reset elemCountIn
        // between parses of individual formulas).
        hasChargeOut = false;
        chargeSignOut = 0;
        for (String compound : out) {
            Formula form = new Formula(compound, elemFact);
            elemCountOut[0] = 0;
            result = form.parse(elemCountOut);
            if (!result) {
                errorMsg = "Invalid formula for product: " + compound;
                break;
            }
            if (elemCountOut[0] != 0) {
                int curSign = (elemCountOut[0] < 0) ? -1 : 1;
                chargeSignOut = hasChargeOut ? ((curSign != chargeSignOut) ? 0 : curSign) : curSign;
                hasChargeOut = true;
            }
        }
        if (!result) {
            return false;
        }

        // Count the number of different elements in the
        // chemical equation. We expect the same elements
        // at the left side and the right side of the eq.
        nElements = (hasChargeIn || hasChargeOut) ? 1 : 0;
        matrixIndex[0] = (hasChargeIn || hasChargeOut) ? 0 : -1;
        int idx;
        for (idx = 1; idx < elemCountIn.length; idx++) {
            if ((elemCountIn[idx] != 0) ^ (elemCountOut[idx] != 0)) {
                break;
            }
            if (elemCountIn[idx] != 0) {
                matrixIndex[idx] = nElements;
                nElements++;
            } else {
                matrixIndex[idx] = -1;
            }
        }
        if (idx < elemCountIn.length) {
            // One of the elements occurs on one side of the
            // eq, while it does not appear on the other side.
            errorMsg = "Element " + elemFact.getElementByNumber(idx).getSym() + " only occurs on one side of equation";
            return false;
        }

        // We also perform a check on the charges. If we have no charge
        // on one side, and all charges on the other side have the same
        // sign, then that is an error. We also have an error if we have
        // charge on both sides, but these charges are of opposite sign.
        if (!hasChargeIn && chargeSignOut != 0) {
            String sgn = (chargeSignOut > 0) ? "positive" : "negative";
            errorMsg = "No charge in reactants, while there only is " + sgn + " charge in the products";
            return false;
        }
        if (!hasChargeOut && chargeSignIn != 0) {
            String sgn = (chargeSignIn > 0) ? "positive" : "negative";
            errorMsg = "No charge in products, while there only is " + sgn + " charge in the reactants";
            return false;
        }
        if (chargeSignIn * chargeSignOut == -1) {
            String sgnIn = (chargeSignIn > 0) ? "positive" : "negative";
            String sgnOut = (chargeSignOut > 0) ? "positive" : "negative";
            errorMsg = "Only " + sgnIn + " charge in reactants, while there only is " + sgnOut + " charge in the products";
            return false;
        }
        // The situation that there is no charge on one side, while there
        // is charge of mixed sign on the other side is accepted. This can
        // occur in reactions where ions combine to molecules or where
        // molecules are split in ions. An example is
        // H2O + HCl --> H3O(+) + Cl(-)

        // Create the coefficient matrix for balancing the
        // equation. Elements at the right are added to the
        // coefficient matrix with a minus sign.
        // The chemical equation can be written in the form
        // Ax = 0, where x is the vector of coefficients of
        // the compounds in the chemical equation. The
        // value of x is determined by determining the null
        // space of the matrix A. For well-defined chemical
        // equations the null-space is one-dimensional and
        // then the coefficients can be determined up to
        // some scaling factor. For some equations, however
        // the null-space may be multi-dimensional. This
        // indicates that certain chemicals may appear in
        // any stoichiometric ratio, as long as the total
        // set of coefficients is a linear combination of
        // the base vectors of the null-space.
        A = new Matrix(nElements, in.length + out.length);
        for (int i = 0; i < in.length; i++) {
            int elemNr;
            for (elemNr = 0; elemNr < elemCountIn.length; elemNr++) {
                elemCountIn[elemNr] = 0;
            }
            Formula form = new Formula(in[i], elemFact);
            form.parse(elemCountIn);
            for (elemNr = 0; elemNr < elemCountIn.length; elemNr++) {
                if (matrixIndex[elemNr] != -1) {
                    A.setElement(matrixIndex[elemNr], i, elemCountIn[elemNr]);
                }
            }
        }

        for (int i = 0; i < out.length; i++) {
            int elemNr;
            for (elemNr = 0; elemNr < elemCountOut.length; elemNr++) {
                elemCountOut[elemNr] = 0;
            }
            Formula form = new Formula(out[i], elemFact);
            form.parse(elemCountOut);
            for (elemNr = 0; elemNr < elemCountOut.length; elemNr++) {
                if (matrixIndex[elemNr] != -1) {
                    A.setElement(matrixIndex[elemNr], in.length + i, -elemCountOut[elemNr]);
                }
            }
        }

        return true;
    }

    
    
    
    
    public boolean solveEquation() {

        // Here we have the matrix A, now we determine the
        // null-space of this matrix. If this null-space
        // is zero-dimensional, then we have an equation,
        // which can only be balanced by using zero-coefs.
        N = A.getKernel();
        if (N == null) {
            // The null space for this reaction is the single
            // point (0 0 0 ... 0). This means that the
            // reaction cannot occur, the equation cannot be 
            // balanced with non-trivial non-zero coefficients.
            errorMsg = "Impossible reaction cannot be balanced";
            return false;
        }

        // The rank of the null space equals the number of linearly
        // independent equations. For completely determined systems
        // we only have one equation, but for some equations the
        // system in under-determined. An example of the latter case
        // is the following reaction:
        //   Cu + HNO3 ---> Cu(NO3)2 + NO + NO2 + H2O
        // The rank of the null space for this equation equals 2.
        int rank = N.getNCols();

        // If the solution space has dimension larger than 1, then
        // recombine basis vectors, such that all elements are
        // positive. It might be that this is not possible. In
        // that case the original solution space is returned.
        if (rank > 1) {
            N = Recombine.recombine(N);
        }

        return true;
    }

    
    
    
    public Matrix getEquation() {
        return A;
    }

    
    
    
    public Matrix getSolution() {
        return N;
    }

    
    
    
    public String getErrorMsg() {
        return errorMsg;
    }

    
    
    
    public Element addPseudoElement(String sym, double mass) {
        return elemFact.addPseudoElement(sym, mass);
    }

    
    
    // A simple program, demonstrating the use of the solver.
    // The reactants are given and the products. The solution
    // space of this reaction is two-dimensional. It can be
    // regarded as a linear combination of two different
    // reactions:
    //    Cu, HNO3 ---> Cu(2+), NO3(-), NO2, --, H2O
    //    Cu, HNO3 ---> Cu(2+), NO3(-), ---, NO, H2O
    //
    // The program gives two solutions (two columns). In this
    // case this corresponds to the two reactions given above,
    // but in general this need not always be the case. A
    // linear combination of the two solution vectors may also
    // be returned.
    //
    // Uncomment the code below for testing.
    
    /*
    public static void main(String args[]) {
        ElementFactory elemFact = new ElementFactory();

        // Specify reactants and products.
        String[] in = new String[]{"Cu", "HNO3"};
        String[] out = new String[]{"Cu(2+)", "NO3(-)", "NO2", "NO", "H2O"};

        // Create a solver.
        Solver solv = new Solver(elemFact, in, out);
        
        // Build the set of equations in the form of a matrix.
        boolean result = solv.buildEquation();
        if (!result) {
            System.out.println(solv.errorMsg);
            return;
        }
        
        // Solve the equations. Get the null-space.
        result = solv.solveEquation();
        if (!result) {
            System.out.println(solv.errorMsg);
            return;
        }

        // Retrieve the null-space and display the elements
        // of the null-space.
        Matrix M = solv.getSolution();
        for (int r = 0; r < M.getNRows(); r++) {
            for (int c = 0; c < M.getNCols(); c++) {
                System.out.print(" " + M.getElement(r, c).getNum().toString());
            }
            System.out.println("");
        }
    }
    */
}
